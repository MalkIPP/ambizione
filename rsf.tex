\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{eurosym}
%\usepackage[a4paper]{geometry}
\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}
\usepackage{setspace}
\onespacing
% Constraints  
    % <=15 pages, everything included
    % < 60 000 characters (with white spaces)
    % min of 10 font size
    % 1.5 line spacing
\usepackage{hyperref}
\usepackage{natbib}	
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage{subcaption}
 \usepackage{multirow}


\begin{document}

\begin{center} 
\textbf{{\Large Tax Complexity and Tax Enforcement in U.S. States: \\ A Text Analysis Approach}}
\end{center}

% =========================================================================================================
% =================
\section*{Introduction}
% =================
%    In the summary (maximum 1 page), please present the background and rationale of the project, list its overall objectives and specific aims, mention the methods to be used, and briefly discuss the expected results and their impact for the field.

A major theme of the empirical literature in public finance is the importance of tax compliance (Andreoni et al, 1998). %EA add to bib
Non-compliance can take the form of evasion, which is in most cases illegal, or avoidance, which is legal. But in both cases, a direct consequence is to decrease the individual tax liability. 

In response to these behaviours, tax systems have implemented enforcement rules. Yet, the economic literature on tax compliance and enforcement has little work identifying a direct causal link between the two. 
The broad goal of this research is to produce empirical evidence on how the tax system as a whole, defined by the tax base and rates as well as by the enforcement mechanisms, have redistributive consequences and affect the behaviours of taxpayers. 

% Both the positive and the normative point of views developed in this field focus on the role and on the perception of a restrictive number of tax parameters. Yet, tax features are extracted from laws that contain much more information than what what is currently used in the  theoretical models and  empirical findings of this field. The broad goal of this research is to produce empirical evidence on how the law related to tax issues determines behaviour. 

To that end, we construct new datasets whose primary input is raw texts such as laws and court cases. Texts are indeed a staple for the real world operating conditions of tax system. We combine this text data with newly available administrative data and original descriptive statistics on tax evasion from government reports. The data work will not so much consist of gathering the text data but rather of prepossessing the text data by extracting information of interest.
%This project being very much data-oriented, a great effort is made toward gathering original datasets. Some are extracted from administrative documents publicly available. Others are new datasets taking the form of collections of text documents (court decisions, budget laws, parliamentary discussions) that will be assembled for the purpose of this project. The last source of data comes from administrative datasets that have recently been made available to researchers in France (corporate tax and income tax data).
%The data used for this project can be grouped into three topics: data on tax offence, data on tax laws and tax return data.

This research plan describes two overlapping projects centered on this approach. 

%1 Tax enforcement in France and in the US 
First, I will study the impact of tax enforcement on tax compliance. I propose to develop new data on evasion, and analyze the text of court cases for fiscal fraud. I will produce a new corpus of court cases the United States and the accompanying metadata on the penalty, the judge, the evading entity, the amount, and the tax evaded. Exogenous variation in enforcement rules will come from the random assignment of judges in the US. The data and resulting analyses will fill a gap by highlighting the role played by tax courts, an institution overlooked in the literature. More importantly, this work will have major implications for the understanding of the impact of tax evasion on the income distribution.

%2 sources of complexity in French tax code

%3 The role of avoidance in the behavioural responses to taxes
Second, I propose to study the role of the tax base in the response to taxation. The definition of the tax base is not easily approachable as it is embedded in the tax code, so I will approach it using text analysis methods. In the context of US state taxation, I will estimate the elasticity of the net-of-tax rate to the taxable income and then look at how it relates to the text embedded measure of the tax base. 
Even though avoidance and evasion are known to have important redistributive consequences, no paper looks precisely at how the definition of the tax base impacts the elasticity. This research will contribute to the public finance literature by clarifying this question. 

This research adds to the empirical literature in public finance and political economy on the individual and political determinants as well as on the redistributive impacts of tax avoidance and evasion, in particular by importing text analysis methodologies. Indeed, this methodology has not been used to contribute to the public finance field despite the major role played by legal texts in taxation. % The results will also have relevant policy implications as
%The public finance literature analyzing the behavioural responses to taxation considers avoidance and evasion but does not study them in detail. These responses to taxation being near the legal frontier, tax enforcement procedures are an important component of their associated cost. Yet, there is a lack of evidence on the operation of tax courts in the tax system, specifically when these institutions relate to tax avoidance. 

%This research consists of two main ideas. 
The rest of the letter follows the following plan.
The first section focuses on the role of tax courts in the legal process in the United States. The second section delves into the complexity of the tax system, by studying the impact of tax enforcement on the level of legal complexity. 
As this research is data-intensive and will involve a heavy data processing part both sections encompass details on the methods for data collection and processing.

% ________________________________________________________________________
\section{Tax Enforcement: the Role of Tax Courts}
% ________________________________________________________________________

The introduction of modern taxation systems in developed countries has historically contributed to progressive policy goals. A key motivation in the 1910s was the need to finance the First World War. After that, fiscal states progressively increased the share of taxpayers in the populations. This became possible because citizens complied with payment of taxes. Yet, tax compliance relies on an enforcement mechanism trusted by citizens. 

The history of tax administration still presents a number of open questions. How did states gain the approval of their citizens? How does the enforcement of tax compliance actually work?  These questions are an emerging theme in the public finance literature on tax enforcement. 

The tax administration literature is mostly theoretical, stemming from the Allingham-Sandmo (1972) model. \cite{Sle.Yit02a} provide an early review of this literature. As illustrated by recent empirical work such as \cite{Sle.Blu.Chr01} and \cite{Kle.Knu.Kre.Ped.Sae11}, the literature is moving toward a more empiricist, process-oriented approach. But still, the empirical literature is somewhat small relative to other areas in public finance.

In particular, there isn't much evidence on the role of tax courts in enforcement. This project addresses the lack of evidence bu studying the role of tax courts in the U.S using data on fiscal frauds coming from the courts. 

\paragraph{Research question} A first objective is to document the evolution of fiscal fraud in the US. We will answer some basic descriptive questions such as: Who is the typical tax evader? individual or firm, rich or poor? What is the preferred evasion strategy? Does the threshold between evasion and avoidance evolve? By how much do tax enforcement procedures matter in efficiently raising tax revenue? A second objective will be to study how the judicial response through the courts' cases related to fraud evolved. I propose an identification strategy relying on the random assignment of judges in the U.S.  

\paragraph{Setting} This part of the project focuses on the role played by tax courts in tax compliance.  In the U.S. there is a dedicated Tax Court for federal taxes, although taxpayers have the option (at some cost) to take cases to general jurisdiction District Courts. The U.S. Tax Court is composed of 19 appointed judges that work in Washington, where the court is based. They also travel nationwide to conduct trials. 

\paragraph{Data Collection}
The main data are the universe of the court cases regarding fiscal fraud in the United States. Court cases can be accessed on legal research databases. 
My colleague Elliott Ash has access to the universe of legal cases in the United States, downloaded from LexisNexis. The cases are published along with contextualizing editorial material. There is information on the subject matter of the case, the sections in the tax code it cites, the previous tax court cases it cites, and the future cases citing the case. But important resources will have to be mobilized for identifying the tax court cases from the universe of court cases as well as for extracting information from the text. Moreover, this first step will be followed by an intensive hand code step aiming to identify whether the case is in favor of the taxpayer or in favor of the tax authority.

\paragraph{Empirical strategy} One important feature of the Tax Court is that, being a federal trial court, cases are randomly assigned to judges. In addition, the cases that are taken to District Courts are also randomly assigned to judges. The random assignment is a recent method for identifying exogenous variations in judicial rulings. It was formalized in \cite{Bel.Che.Che.Han12} and applied by \cite{Ash.Che17}. 
On average, judges tend to rule on similar sets of cases. But judges vary systematically in their decisions -- some judges are pro-taxpayer, you might say, and others are pro-tax-agency. Indeed, judges might have different redistributive preferences, belonging to the conservative or the liberal side of the political spectrum. So the random assignment is equivalent to a shock to the legal component of tax collection rights. This identifying variation will enable me to study the effect of tax enforcement on tax revenue. When judges rule against tax payers does that raise revenues? Do some cases set a precedent by having long-lasting effects on tax revenue?

I will model the effect of judges' severity at the circuit-year level on the tax gap by adapting the framework of \cite{Ash.Che18}. The outcomes $Y_{ijt}$ (for jurisdiction $j$ at time $t$) will be the tax gap, the tax base and tax revenue. 
More formally, the model I intend to estimate is
$$Y_{ijt}=\alpha_{jt}+\rho \mathrm{Law}_{jt}+\epsilon_{jt}$$
$\alpha_{jt}$ includes time and jurisdiction fixed effects and trends as well as time-varying jurisdictions characteristics. $L_{jt}$ is a measure of the law operational in $j$ at $t$.  In \cite{Ash.Che18}, $L_{jt}$ is a scalar measure of the policy direction of decisions issued in jurisdiction $j$ at time $t$. In my context, the simplest measure is the proportion of cases per year that are voted in favor of the taxpayer, versus the tax agency. I will also work on a novel approach for measure $L_{jt}$ using the text of the cases to measure the importance of the case. This would be experimental as no one has really tried to use the text to weight the important opinions. The random assignment will be used to identify exogenous variation in $L_{jt}$. Following \cite{Ash.Che18}, I will use regularized regression (Lasso regression or elastic net for example) to select a set of judge-specific covariates. These variables will be used as instruments in the following first stage equation:
$$L_{jt}=\alpha_{jt}+\gamma Z_{jt}+\eta_{jt}$$
Where $Z_{jt}$ includes the set instrumental variables selected by the regularized regression. 

%--------------------------------------------------------------------------------
%\subsubsection*{Bringing together tax enforcement and tax complexity}
% --------------------------------------------------------------------------------
%The questions of tax enforcement and tax complexity are intricate: it is not clear which is causing the other. Is it tax enforcement enabling more complexity? Does existing complexity require increasing means in tax enforcement? The two previous parts of the project will dig into each of these concepts. I will bring them together in order to look at correlations between the changes in complexity and on enforcement procedures. This descriptive analysis will confirm whether this link actually exists or not. 
%
%Indeed, the more interesting part would be to identify a causal relationship. Are taxes a technological adaptation that increases the efficiency of taxes? 
%
%The theories on tax complexity are still undecided on whether tax systems should avoid complexity as much as possible. One direct effect of tax complexity is to bias agent's behaviours. Governments can take advantage of that in order to raise more tax revenue without distorting more real economic behaviours.   
%On the other side, economic activities could suffer for the illegibility of taxation (for example if tax reforms are too frequent and prevent firms to forecast the total labour cost). If this project would not directly answer this broader question, it would add to the understanding of the trade-off faced by the implementation of complex tax systems by unveiling the role of tax controls and tax courts. 
%%

% ________________________________________________________________________
\section{The Role of Avoidance in the Behavioural Responses to Taxes}
% ________________________________________________________________________

\paragraph{Motivation} Identifying the impact of taxation on income is a central topic in the public finance literature. The key question is whether the level of taxation is detrimental for economic activities. Yet, the tax system consists in several devices that might impact differently the final economic outcome. 
The main idea developed by the literature is to study the relation between the tax rate and the observed tax base (or taxable income). But the definition of the tax base, that can be subject to many rebates and deductions, also has important revenue and redistributive consequences. 

This is why studying the role of the tax base in the response to taxation is important. However, the definition of the tax base is not easily approachable as it is embedded in the tax code. To overcome this issue, this project will rely on the use of methods from text analysis and machine learning. 
On top of that, the definition of the tax base has been heavily used by politicians as it is less salient than changing the tax rates.  Hence, studying how the tax base shapes the responses to taxation is relevant for politics and policy. Indeed, the introduction of loopholes in the tax code enlarges the avoidance possibilities, which binds the hands of future policymakers as increasing tax rates would then trigger higher avoidance response. % might have key policy implications.

\paragraph{Research Question} 

How do tax avoidance possibilities impact the elasticity of taxable income (ETI) with respect to tax rates? How does the political context affect the responses to taxation through the definition of the tax base? For example, do Republicans make avoidance easier? The closest paper is \cite{Ash18}, which analyzes the impact of tax code language on tax revenues for three different taxes: corporate tax, personal income tax and sales tax. I intend to focus on the personal income tax and to use the same methodology for studying a different outcome, which is taxable income. I have to concentrate on the income tax because the computation the ETI requires to use income tax data at the state level that are not available for other taxes.  
The idea is then to predict revenue elasticities (with respect to rates) using tax code text. As a result, I would get  a text-based measure of the legal vehicles for avoidance responses. 
%
\paragraph{Empirical strategy} 
The identification strategy takes advantage of time and spatial variations of taxes. 

The first step is to estimate elasticities of taxable income to the net-of-tax rate\footnote{One minus the tax rate.} by state. \cite{Sae.Sle.Gie12} survey the methodology. They propose a simple strategy for identifying the elasticity with time-series regression. The outcomes are income shares of different income groups. One elasticity will be computed for each state for periods of ten years. 

The second main task is to transform the tax code text into analyzable data. As this step has already been done by \cite{Ash18}, I will largely rely on his work. He then uses topic modelling in order to attribute law statutes to one of the three taxes. I will apply this methodology aiming for identifying deeper elements of the income tax such as rebates, deductions, reductions and tax credits. This method involves some hand code work of the statutes used for training a machine classifier. 
        % Descriptive step: classify the text and link it to the  different features of the tax law (type of income, deduction, rebates, tax credits...)
        
Then I will rely on the strategy proposed by~\cite{Ash18} in order to identify the impact of the effective tax code on elasticities. I will estimate the causal effect of phrases on income shares. He uses as an instrumental variable the lagged regional variation in language within the federal court circuit in order to overcome the fact that the tax code language is chosen endogenously in response to variables that are correlated with tax revenue.
%     $\rightarrow$ determine what are the most predictive phrases relying on
%    Elasticities depend on the current tax systems: as such, it does not make sense to compare ETI coming from systems that encompass different levels of avoidance possibilities. 
\paragraph{Data} 
\begin{itemize}
    \item The state tax rate data are obtained from the World Tax Database and Tax Foundation. The data include information on rates and brackets.
    \item The income shares at the state level are available on \url{https://wid.world/} for the top 10\% and the top 1\% since 1918. Income shares for the rest of the distribution are constructed based on CPS data and are available on \url{http://www.inequalitydata.org/} for years 1963 to 2003.
    \item The text of state tax laws for a 48-year time period (1963-2010) comes from \cite{Ash18}. These laws are modifications of the tax code. % flow 
\end{itemize}

% ===============================
\section{Conclusion and Relevance}
% ===============================
This research will have major impacts in the fields of public finance economics and empirical legal studies using text data. This methodology has not been used to contribute to the public finance field despite the major role played by legal texts in taxation. 
This research will also contribute to fill a gap between the public finance and the political economy literature. Indeed, tax enforcement and tax complexity are studied in political economy, but there has been no direct link with the role of tax avoidance, which is mostly studied in the public finance literature. 
%Indeed, it will lead to the publication of three papers in the public

Moreover, the methodology developed will be easily applicable in other countries as the underlying text documents are most of the time on open access. This means that the research plan might have an important impact in the broader international literature. 

The tax enforcement study will also have important policy implications. Indeed, tax enforcement rules are a constitutive part of the tax system that is frequently subject to reforms. 

% ====================
\bibliographystyle{myagsm}
\bibliography{2018-ambizione-project}

\end{document}

